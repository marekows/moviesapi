from django.db import models
from movies_api.models import Movie
import datetime
from django.utils import timezone

# Create your models here.

class Comment(models.Model):

    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    text = models.TextField()
    created = models.DateTimeField(default=timezone.now)
