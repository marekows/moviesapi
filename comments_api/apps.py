from django.apps import AppConfig


class CommentsSApiConfig(AppConfig):
    name = 'comments_api'
