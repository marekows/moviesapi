from rest_framework import serializers
from .models import Comment


class CommentModelSerializer(serializers.ModelSerializer):

    created = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Comment
        fields = ('movie', 'text', 'created')
