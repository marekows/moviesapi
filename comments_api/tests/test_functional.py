from mixer.backend.django import mixer
from django.test import TestCase
import pytest
from django.urls import reverse
from comments_api.models import Comment
from django.utils import timezone

@pytest.mark.django_db
class TestViews(TestCase):

    def test_add_comment_to_movie(self):

        movie = mixer.blend('movies_api.Movie')
        url = reverse('comments')
        data = {'movie': movie.id, 'text': "test",
                'csrfmiddlewaretoken': 'test'}
        self.client.post(url, data, format='json')

        self.assertEqual(Comment.objects.all().count(), 1)