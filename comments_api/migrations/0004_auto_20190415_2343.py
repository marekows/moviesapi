# Generated by Django 2.2 on 2019-04-15 21:43

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('comments_api', '0003_comment_created'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2019, 4, 15, 23, 43, 27, 535346)),
        ),
    ]
