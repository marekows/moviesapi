from rest_framework.response import Response
from .serializers import CommentModelSerializer
from .models import Comment
from movies_api.models import Movie
from rest_framework import filters, generics

import django_filters.rest_framework


class CommentApiView(generics.ListAPIView):

    queryset = Comment.objects.all()
    filter_fields = ("movie", )
    serializer_class = CommentModelSerializer
    filter_backends = (filters.OrderingFilter,
                       django_filters.rest_framework.DjangoFilterBackend,)
    ordering_fields = ('movie_id', 'created')

    def post(self, request, format=None):

        comment_movie_id = self.fetch_comment_movie_id(request)
        comment_text = self.fetch_comment_text(request)

        movie = self.get_object(comment_movie_id)
        if movie is None:
            return Response({"message": "Movie with that id does not exist"})

        comment = self.add_comment(movie=movie, text=comment_text)
        response = self.comment_response_builder(request, comment, movie)

        return Response(response)
    '''
    When view will stil grow up need to make some seperate
    file for holidng belowed methods
    '''

    def fetch_comment_movie_id(self, request):
        movie_id = request.data["movie"]
        return movie_id

    def fetch_comment_text(self, request):
        text = request.data["text"]
        return text

    def get_object(self, pk):
        try:
            return Movie.objects.get(pk=pk)
        except Movie.DoesNotExist:
            return None

    def add_comment(self, movie, text):
        comment = Comment(movie=movie, text=text)
        comment.save()

        return comment

    def comment_response_builder(self, request, comment, movie):
        response = request.data  # .dict()
        print(type(response).__name__)
        # Belowed loop only needed for using DRF GUI
        if type(response).__name__ != "dict":
            response = response.dict()
            del response["csrfmiddlewaretoken"]
        response["created"] = comment.created
        response["movie_title"] = movie.title
        return response
