# MovieAPI

## Check app online:
http://marekows.pythonanywhere.com/


## Using  
### Movies:
Adding movie:  
Post: `/movies/`, data: `{"title": "<movie_title>"}`  
  
List all movies:    
Get: `/movies/`  

### Comments  
Adding comment  
Post: `/comments/`, data: `{"movie": "<movie_id>", "text": "<comment_text>"}`  
  
List comments:    
Get: `/comments/`    
Ordering from newest: get `/comments/?ordering=-created`    
Ordering from oldest: get `/comments/?ordering=created`    
By movie: get `/comments/?movie=<movie_id>`    
  
### Top Commented in specific date  
You need to provide date range. Only one limitation date can be passed.  
Get `/top/?created=&created_min=<year-month-day>&created_max=<year-month-day>`  
Samples:    
`http://127.0.0.1:8000/top/?created=&created_min=2019-04-13&created_max=2019-04-14`  
`http://127.0.0.1:8000/top/?created=&created_min=2019-04-13`  

## For installing it:
1. Execute: `git clone https://marekows@bitbucket.org/marekows/moviesapi.git`
2. Create python virtual environment
3. Active you venv
4. Go to project dir
5. Execute: `pip install -r requirements.txt`
6. Fallow instruction in 'api_project/settings.py (lines: 19-52) for providing secrets data!!! You must provide your api key for http://www.omdbapi.com/
7. Execute: `python manage.py migrate`
8. Execute: `python manage.py runserver`
9. For running test: `py.test`


### Concerns about the code (resulting from my time limitation):
- app needs refactor views, they are to big. eg. make some utilis file for common methods
- refactor test (use common methods) and for sure more testing should be done
- for sure it should be more comments inside code 