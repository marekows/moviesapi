"""
Django settings for api_project project.

Generated by 'django-admin startproject' using Django 2.2.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""
from configparser import RawConfigParser
import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# On Production Env!!!!
# Create settings.ini file for storing relevant vars in safe way
# For Testing Env you don't need to created the file. Just uncomment
# 3 belowed parameters and set "API_KEY_OMDBAPI" correctly
#API_KEY_OMDBAPI = 'xxxxxxxx'
#DEBUG = True
#ALLOWED_HOSTS = []

'''
Cheatscheet for settings.ini
cat /etc/settings.ini

[secrets]
API_KEY_OMDBAPI: xxxxxxx   # key for OMDBAPIexternal api
SERVER_TYPE:  production   # production or other value for non-production env
DOMAIN_NAME: example.hosting.com # empty if local server
PATH_TO_STATIC: /home/<username>/MovieAPI/static  # empty if local server
'''

# Comment belowed if settings.ini not used
config = RawConfigParser()
# Set correct path to your settings.ini file
config.read('/etc/settings.ini')

API_KEY_OMDBAPI = config.get('secrets', 'API_KEY_OMDBAPI')
SERVER_TYPE = config.get('secrets', 'SERVER_TYPE')

if SERVER_TYPE == 'production':
    DEBUG = False
    ALLOWED_HOSTS = ['%s' % config.get('secrets', 'DOMAIN_NAME')]
    STATIC_ROOT = '%s' % config.get('secrets', 'PATH_TO_STATIC')
else:
    DEBUG = True
    ALLOWED_HOSTS = []

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '$9fyfml-ys@_=3fr82_q0n$xaqn53ze-gyxf7gg4#c8*^z1w$s'

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'django_filters',
    'movies_api',
    'comments_api',
    'ranking_api',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'api_project.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'api_project.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

USE_TZ = False
TIME_ZONE = 'Europe/Paris'

USE_I18N = True

USE_L10N = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/static/'

REST_FRAMEWORK = {
    'DEFAULT_FILTER_BACKENDS': ('django_filters.rest_framework.DjangoFilterBackend',)
}
