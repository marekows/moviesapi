from rest_framework.response import Response
from rest_framework import generics
from rest_framework.exceptions import APIException
from .serializers import TopCommentsSerializer
from comments_api.models import Comment
from django.db.models import Count
from django_filters import rest_framework as filters
from django.db.models import Max


class MyFilter(filters.FilterSet):
    created_min = filters.DateFilter('created', lookup_expr='gte')
    created_max = filters.DateFilter('created', lookup_expr='lte')


class RankApiView(generics.ListAPIView):

    queryset = Comment.objects.all()
    serializer_class = TopCommentsSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = MyFilter

    def get(self, request):

        created_min = self.request.query_params.get('created_min')
        created_max = self.request.query_params.get('created_max')

        if created_min and created_max:
            comments_in_range = Comment.objects.filter(
                created__gte=created_min, created__lte=created_max)
        elif not created_min and created_max:
            comments_in_range = Comment.objects.filter(
                created__lte=created_max)
        elif created_min and not created_max:
            comments_in_range = Comment.objects.filter(
                created__gte=created_min)
        else:
            raise APIException(
                'Provide a data range in format: '
                '?created=&created_min=2019-04-13&'
                'created_max=2019-04-14, At least one date filter is required')

        q = comments_in_range.values('movie_id').annotate(
            comments_count=Count('movie_id'))
        q_max = q.aggregate(Max('comments_count'))

        serializer = TopCommentsSerializer(
            q, many=True, context={'max_comments':
                                   q_max['comments_count__max']})

        return Response(serializer.data)
