from rest_framework import serializers


class TopCommentsSerializer(serializers.Serializer):

    rank = serializers.SerializerMethodField('get_ranking')

    def get_ranking(self, obj):
        '''
        on the basis of the film with the most comments,
        a proportional ranking is created.
        Ranking in the range 1 (top commented) to 5
        '''
        rank_1 = self.context['max_comments']
        obj_comments_count = obj['comments_count']
        rank = obj_comments_count / rank_1 * 5 - 6
        rank = int(abs(rank))
        return rank

    movie_id = serializers.CharField()
    comments_count = serializers.IntegerField(allow_null=True)
