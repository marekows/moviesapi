# Generated by Django 2.2 on 2019-04-12 22:26

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('movies_api', '0004_auto_20190412_2225'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='movie',
            name='plot',
        ),
    ]
