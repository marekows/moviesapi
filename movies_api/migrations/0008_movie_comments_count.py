# Generated by Django 2.2 on 2019-04-13 13:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('movies_api', '0007_delete_comment'),
    ]

    operations = [
        migrations.AddField(
            model_name='movie',
            name='comments_count',
            field=models.IntegerField(default=0),
        ),
    ]
