# Generated by Django 2.2 on 2019-04-12 22:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('movies_api', '0002_auto_20190412_2221'),
    ]

    operations = [
        migrations.AlterField(
            model_name='movie',
            name='imdbVotes',
            field=models.FloatField(),
        ),
    ]
