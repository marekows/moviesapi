from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import MoviesSerializer, MoviesModelSerializer
from .models import Movie
import requests
from rest_framework.exceptions import APIException
from django.conf import settings
from django.db import IntegrityError

class MovieApiView(APIView):

    serializer_class = MoviesSerializer

    def get(self, request):

        movies = Movie.objects.all()
        serializer = MoviesModelSerializer(movies, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):

        self.title = request.data["title"]
        if self.is_title_presence() is False:
            return Response({"message": "Movie title is required"})

        self.title = self.refactor_title()
        movie_response = self.fetch_movie_data()

        if self.is_movie_exist(movie_response) is True:
            self.add_movie(movie_response)
            return Response(movie_response)
        else:
            return Response({"message": 'Movie not found'})

    def is_title_presence(self):

        if self.title == "":
            return False
        return True

    def refactor_title(self):
        '''
        For using external Movie API: spaces (" ")
        in movie title needed to be change for "+"
        '''
        refactored_title = self.title.replace(" ", "+")
        return refactored_title

    def fetch_movie_data(self):

        key = settings.API_KEY_OMDBAPI
        mv = requests.get(
            'http://www.omdbapi.com/?t=%s&apikey=%s' % (self.title, key))
        response = mv.json()
        return response

    def is_movie_exist(self, movie):

        if movie["Response"] == 'True':
            return True
        elif movie["Error"] == 'Invalid API key!':
            raise APIException(
                'Problem with connecting '
                'to external API. Sorry!!!')
        else:
            return False

    def add_movie(self, movie_data):
        title = movie_data['Title']
        year = movie_data['Year']
        imdbRating = movie_data['imdbRating']
        if imdbRating == "N/A":
            imdbRating = 0
        production = movie_data['Production']

        movie = Movie(title=title, year=year, imdbRating=imdbRating,
                      production=production)
        try:
            movie.save()
        except IntegrityError:
            raise APIException(
                'Movie already added')
