from django.urls import reverse
from rest_framework.test import APITestCase
from movies_api.models import Movie
from django.conf import settings
import requests


class MoviesTests(APITestCase):

    def test_create_movie(self):

        url = reverse('movies')
        data = {'title': 'gladiator'}
        self.client.post(url, data, format='json')
        self.assertEqual(len(Movie.objects.filter(title='Gladiator')), 1)

    def test_full_movie_data_response_after_created_movie(self):

        movie_title = 'gladiator'
        url = reverse('movies')
        data = {'title': movie_title}

        app_response = self.client.post(url, data, format='json')

        external_api_response = self.get_movie(movie_title)

        self.assertEqual(app_response.json(), external_api_response.json())

    def test_movie_not_found(self):

        movie_title = 'asdfzxcvqwert'
        url = reverse('movies')
        data = {'title': movie_title}

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.json(), {"message": 'Movie not found'})

    def test_wrong_external_api_key(self):
        external_api_response = self.get_movie(key="1q2w3e4r5t")
        self.assertEqual(external_api_response.json(), {
                         'Error': 'Invalid API key!', 'Response': 'False'})

    def get_movie(self, movie_title="shrek", key=settings.API_KEY_OMDBAPI):
        external_api_response = requests.get(
            'http://www.omdbapi.com/?t=%s&apikey=%s' % (movie_title, key))
        return external_api_response
