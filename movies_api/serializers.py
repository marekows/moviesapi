from rest_framework import serializers
from .models import Movie


class MoviesSerializer(serializers.Serializer):
    title = serializers.CharField()


class MoviesModelSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Movie
        fields = ('title', 'year', 'imdbRating',
                  'production',)
