from django.db import models


class Movie(models.Model):

    title = models.CharField(max_length=50, unique=True)
    year = models.IntegerField()
    imdbRating = models.FloatField()
    production = models.CharField(max_length=50)

    class Meta:
        ordering = ['-imdbRating']

    def __str__(self):
        return self.title
